# Storefront

Storefront of a shop developed by django.

## Features

- **Data are stored in PostgreSQL**
- **Django template and REST framework**
- **Customized admin**
- **Object Relational Mapping**
- **Serializer and Deserializer**

## Pictures
 <p align="center">
    Database
    <br\><br\>
    <img src = "pics/DB_tables.png" alt="Database tables">
  </p>
  <br\>
  <p align="center">
    Admin Panel
    <br\><br\>
    <img src = "pics/admin panel.png" alt="admin panel">
  </p>
  <br\>
  <p align="center">
    <img src = "pics/admin c.png" alt="admin customization">
  </p>
  <br\>
  <p align="center">
    Django REST framework
    <br\><br\>
    <img src = "pics/drf1.png" alt="rest framework">
  </p>
<br\>
 <p align="center">
    POST
    <br\><br\>
    <img src = "pics/post.png" alt="POST">
  </p>
  <br\>
   <p align="center">
    DELETE, UPDATE(PUT)
    <br\><br\>
    <img src = "pics/delete, put.png" alt="DELETE, PUT">
  </p>
  <br\>   
